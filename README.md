# Iniciativa Pokemon True Colors - Progressive Web Application (PWA)

Trata-se de um projeto de Open Source (de código aberto) feito por fãs da fraquia pokemon que buscam incluir experiencias divergentes da jogos oficiais.

## Brain Storming

Criamos um documento para brain storming no Google Jamboard

[Brain Storming Jamboard](https://jamboard.google.com/d/1w8KqLjwIh2kJYH1i9ytb4LAu8N1xfFkoAbj_FXoZDWs/viewer?f=1)

## Tecnologias e Ferramentas envolvidas

Abaixo descrevemos algumas tecnologias que estamos usando para quem se interessar em participar do projeto. Todas as ferramentas são gratuitas e procuramos usar o máximo de recursos Open Source e livre.

### Tecnologias e conhecimentos envolvidos

  * HTML5, CSS e Javascript 
  * [PWA](https://blog.rocketseat.com.br/pwa-o-que-e-quando-utilizar/)
  * [A-Frame](https://aframe.io/)

### Ferramentas

  * [Blender](blender.org/) - Para modelagem e animação 3D
  * [glitch](https://glitch.com) - Para edição de código direto do navegador
